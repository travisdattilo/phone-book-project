/*  Travis Dattilo
    cssc0886
*/

package data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException; 

public class BinarySearchTree<K extends Comparable <K>,V> implements DictionaryADT<K,V> {
	private Node<K,V> root;
	private int currentSize;
	private long modCounter;
	private K reverseLookupTmp;	//for finding keys in reverse lookup
	
	class Node<K,V>{
		private K key;
		private V value;
		private Node<K,V> leftChild, rightChild;
		
		public Node(K k, V v){
			key = k;
			value = v;
			leftChild = rightChild = null;
		}
	}
	 
	public BinarySearchTree(){
		root = null;
		reverseLookupTmp = null;
		currentSize = 0;
		modCounter = 0;
	}
	
	// Returns true if the dictionary has an object identified by
	// key in it, otherwise false.
	public boolean contains(K key){
		return getValue(key) != null;
	}
		
	// Adds the given key/value pair to the dictionary.  Returns 
	// false if the dictionary is full, or if the key is a duplicate.
	// Returns true if addition succeeded.
	public boolean add(K key, V value){
		if(contains(key)) return false;
		if(root == null) root = new Node <K,V>(key, value);
		else insert(key, value, root, null, false);
		currentSize++;
		modCounter++;
		return true;
	}
	
	private void insert(K key, V value, Node<K,V> n, Node<K,V> parent, boolean goLeft){
		if(n == null) {
			if(goLeft) parent.leftChild = new Node<K,V>(key,value);
			else parent.rightChild = new Node<K,V>(key,value);
		}
		else if(key.compareTo(n.key) < 0) insert(key,value,n.leftChild,n,true);
		else insert(key, value, n.rightChild, n, false);			
	}
	
	// Deletes the key/value pair identified by the key parameter.
	// Returns true if the key/value pair was found and removed,
	// otherwise false.
	public boolean delete(K key){
		//if(root == null) return false;
		if(isEmpty()) return false;
		if(!delete(key, root, null, false)) return false;
		currentSize--;
		modCounter++;
		return true;

	}
	
	private boolean delete(K key, Node<K,V> n, Node<K,V> parent, boolean wentLeft){ 
		if(n == null) return false;
		else if(key.compareTo(n.key) < 0) return delete(key, n.leftChild, n, true);	//to the left
		else if (key.compareTo(n.key) > 0) return delete(key, n.rightChild, n, false); //to the right
		else{	//found case
			if(n.leftChild == null && n.rightChild == null){	//to delete has no children
				if(parent == null) root = null;						
				else if(wentLeft) parent.leftChild = null;			
				else parent.rightChild = null; 						
			}
			else if(n.leftChild == null) {						// to delete has one right child
				if(parent == null) root = n.rightChild;				
				else if(wentLeft) parent.leftChild = n.rightChild;
				else parent.rightChild = n.rightChild;
			}
			else if(n.rightChild == null){				// to delete has one left child
				if(parent == null) root = n.leftChild;
				else if (wentLeft) parent.leftChild = n.leftChild;
				else parent.rightChild = n.leftChild;
			}
			else {	// to delete has two children
				Node<K,V> tmp = getSuccessor(n);
				if(tmp != null){
					n.key = tmp.key;
					n.value = tmp.value;
				}
				else{
					Node<K,V> tmpRight = n.rightChild;
					n.key = tmpRight.key; 
					n.value = tmpRight.value;
					n.rightChild = tmpRight.rightChild;
				}
			}
		}
		return true; 
	}
	
	private Node<K,V> getSuccessor(Node<K,V> n){
		Node<K,V> parent = null;
		Node<K,V> current = n.rightChild;
		while(current.leftChild != null){
			parent = current;
			current = parent.leftChild;
		}
		if(parent == null)return null;
		parent.leftChild = current.rightChild;
		return current;
	}
	
	// Returns the value associated with the parameter key.  Returns
	// null if the key is not found or the dictionary is empty.
	public V getValue(K key){
		return findValue(key, root);
	}
	
	private V findValue(K key, Node<K,V> n){
		if(n == null) return null;
		if(key.compareTo(n.key) < 0) return findValue(key, n.leftChild); 	//go left
		if(key.compareTo(n.key) > 0) return findValue(key, n.rightChild);	//go right
		return n.value;
	}
	
	// Returns the key associated with the parameter value.  Returns
	// null if the value is not found in the dictionary.  If more 
	// than one key exists that matches the given value, returns the
	// first one found. 
	public K getKey(V value){	//reverse lookup
		reverseLookupTmp = null;
		return find(root, value);
	}
	
	private K find(Node<K,V> n, V value){	//reverse lookup helper find method
		if(n == null) return null;
		if(((Comparable<V>)n.value).compareTo(value) == 0) reverseLookupTmp = n.key;
		find(n.leftChild, value);	//go left
		find(n.rightChild, value);	//go right
		return reverseLookupTmp;
	}
		
	// Returns the number of key/value pairs currently stored 
	// in the dictionary
	public int size(){
		return currentSize;
	}
	
	// Returns true if the dictionary is at max capacity
	public boolean isFull(){
		return false;
	}
	
	// Returns true if the dictionary is empty
	public boolean isEmpty(){
		return currentSize == 0;
	}
		
	// Returns the Dictionary object to an empty state.
	public void clear(){
		currentSize = 0;
		root = null;
	}
	
	// Returns an Iterator of the keys in the dictionary, in ascending
	// sorted order.  The iterator must be fail-fast.
	public Iterator keys(){
		return new KeyIteratorHelper();
	}
	
	// Returns an Iterator of the values in the dictionary.  The
	// order of the values must match the order of the keys. 
	// The iterator must be fail-fast. 
	public Iterator values(){
		return new ValueIteratorHelper();
	}
	
	class KeyIteratorHelper extends IteratorHelper<K>{
		public KeyIteratorHelper(){
			super();
		}
		public K next(){
			if(!hasNext()){
				throw new NoSuchElementException();
			}
			return nodeArray[iter++].key;
		}
	}
	
	class ValueIteratorHelper extends IteratorHelper<V>{
		public ValueIteratorHelper(){
			super();
		}
		
		public V next(){
			if(!hasNext()){
				throw new NoSuchElementException();
			}
			return nodeArray[iter++].value;
		}
	}
	
	abstract class IteratorHelper<E> implements Iterator<E>{
		Node<K,V>[] nodeArray;
		long mod;
		int iter;
		
		public IteratorHelper(){
			nodeArray = new Node[currentSize];
			iter = 0;
			mod = modCounter;
			load(root);	// load the array starting at the root then walk
			iter = 0;
		}
		
		public void load(Node<K,V> node){	//walk the tree, populates the array
			if(node == null) return;
			load(node.leftChild);
			nodeArray[iter++] = node;
			load(node.rightChild);
		}
		
		public boolean hasNext() {
			if(mod != modCounter) throw new ConcurrentModificationException();
			return iter < currentSize;
		}
		public abstract E next();
		
		public void remove(){
			throw new UnsupportedOperationException();
		}
	}
}