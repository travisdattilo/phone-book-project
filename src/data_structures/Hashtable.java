/*  Travis Dattilo
    cssc0886
*/

package data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException; 
import java.util.NoSuchElementException;

public class Hashtable<K extends Comparable <K>,V> implements DictionaryADT<K,V>{
		private long modCounter;
		private int currentSize, maxSize, tableSize;
		private LinearList<DictionaryNode<K,V>>[] list; 
			
		public Hashtable(int size){
			currentSize = 0;
			maxSize = size;
			modCounter = 0;
			tableSize = (int)(maxSize *1.3f);
			list = new LinearList[tableSize];
			for(int i = 0; i < tableSize; i++)
				list[i] = new LinearList<DictionaryNode<K,V>>();
		}
		
		private int getIndex(K key){
			return (key.hashCode() & 0x7FFFFFFF) % tableSize;
		}
		
		// Returns true if the dictionary has an object identified by
		// key in it, otherwise false.
		public boolean contains(K key){
			return list[getIndex(key)].contains(new DictionaryNode<K,V>(key,null));
		}

		// Adds the given key/value pair to the dictionary.  Returns 
		// false if the dictionary is full, or if the key is a duplicate.
		// Returns true if addition succeeded.
		public boolean add(K key, V value){
			if(isFull()) 
				return false;
			if(list[getIndex(key)].contains(new DictionaryNode<K,V>(key, null)))
				return false;
			list[getIndex(key)].addFirst(new DictionaryNode<K,V>(key,value));
			currentSize++;
			modCounter++;
			return true;			
		}

		// Deletes the key/value pair identified by the key parameter.
		// Returns true if the key/value pair was found and removed,
		// otherwise false.
		public boolean delete(K key){
			if(list[getIndex(key)].remove(new DictionaryNode<K,V>(key, null)) != null){
				currentSize--;
				modCounter++;
				return true;
			}
			return false;
		}

		// Returns the value associated with the parameter key.  Returns
		// null if the key is not found or the dictionary is empty.
		public V getValue(K key){
			if(isEmpty()) return null;
			DictionaryNode<K,V> tmp = list[getIndex(key)].find(new DictionaryNode <K,V>(key,null));
			if(tmp == null) return null;
			return tmp.value;
		}

		// Returns the key associated with the parameter value.  Returns
		// null if the value is not found in the dictionary.  If more 
		// than one key exists that matches the given value, returns the
		// first one found. 
		public K getKey(V value){
			for(int i = 0; i < tableSize; i++){
				for(DictionaryNode<K,V> n : list[i]){
					if(((Comparable<V>)value).compareTo(n.value) == 0){
						return n.key;
					}
				}
			}
			return null;
		}

		// Returns the number of key/value pairs currently stored 
		// in the dictionary
		public int size(){
			return currentSize;
		}

		// Returns true if the dictionary is at max capacity
		public boolean isFull(){
			return currentSize == maxSize;
		}

		// Returns true if the dictionary is empty
		public boolean isEmpty(){
			return currentSize == 0;
		}

		// Returns the Dictionary object to an empty state.
		public void clear(){
			currentSize = 0;
			modCounter = 0;
			for(int i = 0; i < tableSize; i++)
				list[i].clear();
		}
		
		//Wrapper Class
		class DictionaryNode<K,V> implements Comparable<DictionaryNode<K,V>>{
			K key; 
			V value;
		public DictionaryNode (K key, V value){
				this.key = key;
				this.value = value;
			}
			public int compareTo(DictionaryNode<K,V> Node){
					return ((Comparable<K>)key).compareTo((K)Node.key);
			}
		}
		
		// Returns an Iterator of the keys in the dictionary, in ascending
		// sorted order.  The iterator must be fail-fast.
		public Iterator<K> keys(){
			return new KeyIteratorHelper();
		}

		// Returns an Iterator of the values in the dictionary.  The
		// order of the values must match the order of the keys. 
		// The iterator must be fail-fast. 
		public Iterator<V> values(){
			return new ValueIteratorHelper(); 
		}
		
		abstract class IteratorHelper<E> implements Iterator<E>{
			protected DictionaryNode<K,V> [] tmpArray;
			protected int idx;
			protected long modCheck;
			
			public IteratorHelper(){
				tmpArray = new DictionaryNode[currentSize];
				idx = 0;
				int index = 0;
				modCheck = modCounter;
				for(int i = 0; i < tableSize; i++)
					for(DictionaryNode tmp : list[i])
						tmpArray[index++]= tmp;
				shellSort();
			}
			
			public boolean hasNext(){
				if(modCheck != modCounter)
					throw new ConcurrentModificationException();
				return idx < currentSize;
			}
			
			public abstract E next(); //not used
			
			public void remove() {
				throw new UnsupportedOperationException();	
			}
			
			private void shellSort() {
				int in, out, h = 1;
				DictionaryNode<K,V> tmp;
				int size = tmpArray.length;
				while(h <= size/3) //calculate gaps
					h = h*3+1;
				while(h > 0){
					for(out = h; out < size; out++){
						tmp = tmpArray[out];
						in = out;
						while(in > h-1 && (tmpArray[in-h]).compareTo(tmp) >= 0){	
							tmpArray[in] = tmpArray[in-h];
							in -= h;
						}
						tmpArray[in] = tmp;
					}
					h = (h-1)/3;
				}
			}
		}
		
		class KeyIteratorHelper<K> extends IteratorHelper<K>{
			public KeyIteratorHelper(){
				super();
			}
			public K next(){
				if(!hasNext()){
					throw new NoSuchElementException();
				}
				return (K) tmpArray[idx++].key; 
			}
		}
		
		class ValueIteratorHelper<V> extends IteratorHelper<V>{
			public ValueIteratorHelper(){
				super();
			}
			public V next(){
				if(!hasNext()){
					throw new NoSuchElementException();
				}
				return (V) tmpArray[idx++].value;
			}
		}

}
