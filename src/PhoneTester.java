
public class PhoneTester {
    public static void main(String[] args) {
    	
//    	******************************
//   	*							 *
//    	*   My Personal Phone Book   *
//   	*							 *
//    	******************************
      	
        PhoneBook mine = new PhoneBook(new Integer(20000));
      
        mine.addEntry(new PhoneNumber("408-123-4567"), "Dattilo, Travis");
        mine.addEntry(new PhoneNumber("408-234-5678"), "Dattilo, John");
        mine.addEntry(new PhoneNumber("408-345-6789"), "Dattilo, Kim");
        mine.addEntry(new PhoneNumber("408-456-7891"), "Robbins, Baskin");
        mine.addEntry(new PhoneNumber("408-567-8910"), "Matts, Caitlin");
        mine.addEntry(new PhoneNumber("408-678-9101"), "Dattilo, Drew");
        mine.addEntry(new PhoneNumber("619-565-5050"), "Dattilo, Travis");
        
        System.out.println("");
        
        System.out.println("All items in the phonebook below this line");
        mine.printAll(); 
        System.out.println("End of list of all items in the phonebook");
        
        System.out.println("");

        
        System.out.println(mine.numberLookup(new PhoneNumber("408-123-4567")));
        System.out.println(mine.nameLookup("Dattilo, Travis"));
                
        mine.deleteEntry(new PhoneNumber("408-123-4567"));
        
        System.out.println("");

        
        System.out.println("All items in the phonebook below this line");
        mine.printAll(); 
        System.out.println("End of list of all items in the phonebook");
        
        System.out.println("");
        
        
        mine.printByAreaCode("408");
        
        System.out.println(mine.numberLookup(new PhoneNumber("619-565-5050")));		
        System.out.println(mine.nameLookup("Dattilo, Travis"));
        
        System.out.println("");       
        
// ====================================================================================================== \\        
        System.out.println("======================================================================================================");
        
//    	******************************
//   	*							 *
//    	*   Short Phone Book File    *
//   	*							 *
//    	******************************
        System.out.println("");
        
        PhoneBook small = new PhoneBook(new Integer(20000));
        
        small.load("p4_short_data.txt");
        
        System.out.println("All items in the phonebook below this line");
        small.printAll(); 
        System.out.println("End of list of all items in the phonebook");
               
        System.out.println(small.numberLookup(new PhoneNumber("195-516-1519")));		
        System.out.println(small.nameLookup("Pearson, Don"));
        
        System.out.println(small.numberLookup(new PhoneNumber("692-494-1183")));		
        System.out.println(small.nameLookup("Trenton, Bill"));
        
        System.out.println(small.numberLookup(new PhoneNumber("025-429-7897")));		
        System.out.println(small.nameLookup("Trenton, Torri"));
        
        System.out.println(small.numberLookup(new PhoneNumber("304-853-0376")));		
        System.out.println(small.nameLookup("Grund, Jason"));
        
        System.out.println(small.numberLookup(new PhoneNumber("505-600-8090")));		
        System.out.println(small.nameLookup("Roberts, Mike"));
        
        System.out.println(small.numberLookup(new PhoneNumber("063-150-2434")));		
        System.out.println(small.nameLookup("Smith, Mike"));
        
        System.out.println(small.numberLookup(new PhoneNumber("133-971-2800")));		
        System.out.println(small.nameLookup("Jacobson, Marie"));
        
        System.out.println(small.numberLookup(new PhoneNumber("265-962-6618")));
        System.out.println(small.nameLookup("Roberts, Murray"));

        small.deleteEntry(new PhoneNumber("265-962-6618"));
        
        System.out.println("");
        
        System.out.println("All items in the phonebook below this line");
        small.printAll(); 
        System.out.println("End of list of all items in the phonebook");
        System.out.println("");
        
     // ====================================================================================================== \\        
        System.out.println("======================================================================================================");
        
//    	******************************
//   	*							 *
//    	*   Full Phone Book File     *
//   	*							 *
//    	******************************
        System.out.println("");     
        
        
        PhoneBook full = new PhoneBook(new Integer(20000));
        
        full.load("p4_data.txt");
       
        
        full.printByAreaCode("619");
        System.out.println("");        
        full.printByAreaCode("133");
        
        full.printAll();
 
        System.out.println("");        

        
        System.out.println(full.numberLookup(new PhoneNumber("061-865-4383")));		
        System.out.println(full.nameLookup("Ransom, Don"));
        
    }
}