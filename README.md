## Project Overview and Included Files

*LinearList* is an array structure implemented from the *LinearListADT* interface. From this I created the *Hashtable* class which uses an instance of the *LinearList* and new functions to maintain a hashtable's key/value pairs. The *Hashtable* class uses the *DictionaryADT* interface. I additionally created a *BinarySearchTree* and *BalancedTree* (red/black tree) class, both of which implement the *DictionaryADT* interface. The *PhoneBook* class includes a number of functions that utilizes the *Hashtable* to create a phone book dictionary. The *PhoneNumber* class is to verify phone numbers. The other two programs, *DictionaryTester* and *PhoneTester* are drivers/example use cases of the previously listed programs. 

- **LinearList.java**
- **LinearListADT.java**
- **DictionaryADT.java**
- **Hashtable.java**
- **BinarySearchTree.java**
- **BalancedTree.java**
- **PhoneNumber.java**
- **PhoneBook.java** 
- **DictionaryTester.java**
- **PhoneTester.java**

## LinearList Functions

- **addFirst:** Adds the Object obj to the beginning of list and returns true if the list is not full. Returns false and aborts the insertion if the list is full.
- **addLast:** Adds the Object obj to the end of list and returns true if the list is not full. Returns false and aborts the insertion if the list is full.
- **removeFirst:** Removes and returns the parameter object obj in first position in list if the list is not empty, null if the list is empty.
- **removeLast:** Removes and returns the parameter object obj in last position in list if the list is not empty, null if the list is empty.
- **remove:** Removes and returns the parameter object obj from the list if the list contains it, null otherwise. The ordering of the list is preserved. The list may contain duplicate elements. This method removes and returns the first matching element found when traversing the list from first position. Shift elements to fill in the slot where the deleted element was located.
- **peekFirst:** Returns the first element in the list, null if the list is empty. The list is not modified.
- **peekLast:** Returns the last element in the list, null if the list is empty. The list is not modified.
- **contains:** Returns true if the parameter object obj is in the list, false otherwise. The list is not modified.
- **find:** Returns the element matching obj if it is in the list, null otherwise. In the case of duplicates, this method returns the element closest to front. The list is not modified.
- **clear:** The list is returned to an empty state.
- **isEmpty:** Returns true if the list is empty, otherwise false
- **isFull:** Returns true if the list is full, otherwise false
- **size:** Returns the number of Objects currently in the list.
- **iterator:** Returns an Iterator of the values in the list, presented in the same order as the underlying order of the list. (front first, rear last)

## Hashtable, BinarySearchTree, and BalancedTree Functions

- **contains:** Returns true if the dictionary has an object identified by key in it, otherwise false.
- **add:** Adds the given key/value pair to the dictionary.  Returns false if the dictionary is full, or if the key is a duplicate. Returns true if addition succeeded.
- **delete:** Deletes the key/value pair identified by the key parameter. Returns true if the key/value pair was found and removed, otherwise false.
- **getValue:** Returns the value associated with the parameter key. Returns null if the key is not found or the dictionary is empty.
- **getKey:** Returns the key associated with the parameter value.  Returns null if the value is not found in the dictionary.  If more than one key exists that matches the given value, returns the first one found. 
- **size:** Returns the number of key/value pairs currently stored in the dictionary
- **isFull:** Returns true if the dictionary is at max capacity
- **isEmpty:** Returns true if the dictionary is empty
- **clear:** Returns the Dictionary object to an empty state.
- **keys:** Returns an Iterator of the values in the dictionary.  The order of the values must match the order of the keys. The iterator must be fail-fast. 

## PhoneNumber Functions

- **compareTo:** Follows the specifications of the Comparable Interface.
- **hashCode:** Returns an int representing the hashCode of the PhoneNumber.
- **verify:** Private method to validate the Phone Number.  Should be called from the constructor.
- **getAreaCode:** Returns the area code of the Phone Number.
- **getPrefix:** Returns the prefix of the Phone Number.
- **getNumber:** Returns the the last four digits of the number.
- **toString:** Returns the Phone Number. 

## PhoneBook Functions

- **load:** Reads PhoneBook data from a text file and loads the data into the PhoneBook.  Data is in the form "key=value" where a phoneNumber is the key and a name in the format "Last, First" is the value.
- **numberLookup:** Returns the name associated with the given PhoneNumber, if it is in the PhoneBook, null if it is not.
- **nameLookup:** Returns the PhoneNumber associated with the given name value. There may be duplicate values, return the first one found. Return null if the name is not in the PhoneBook.
- **addEntry:** Adds a new PhoneNumber = name pair to the PhoneBook.  All names should be in the form "Last, First". Duplicate entries are *not* allowed.  Return true if the insertion succeeds otherwise false (PhoneBook is full or the new record is a duplicate).  Does not change the datafile on disk.
- **deleteEntry:** Deletes the record associated with the PhoneNumber if it is in the PhoneBook.  Returns true if the number was found and its record deleted, otherwise false.  Does not change the datafile on disk.
- **printAll:** Prints a directory of all PhoneNumbers with their associated names, in sorted order (ordered by PhoneNumber).
- **printByAreaCode:** Prints all records with the given Area Code in ordered sorted by PhoneNumber.
- **printNames:** Prints all of the names in the directory, in sorted order (by name, not by number).  There may be duplicates as these are the values. 
