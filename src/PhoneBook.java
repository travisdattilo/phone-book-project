/*  Travis Dattilo
    cssc0886
*/

import data_structures.*;
import java.util.Iterator;
import java.io.*;

public class PhoneBook {
	private int maxSize;
	private DictionaryADT<PhoneNumber, String> theBook;
		
    // Constructor.  There is no argument-less constructor, or default size
    public PhoneBook(int maxSize){
    	this.maxSize = maxSize;
    	theBook = new Hashtable<PhoneNumber, String>(maxSize);
    	//theBook = new BinarySearchTree<PhoneNumber, String>();
    	//theBook = new BalancedTree<PhoneNumber, String>();	
    }
       
    // Reads PhoneBook data from a text file and loads the data into
    // the PhoneBook.  Data is in the form "key=value" where a phoneNumber
    // is the key and a name in the format "Last, First" is the value.
    public void load(String filename){
    	String line;
    	try{
    		BufferedReader in = new BufferedReader(new FileReader(filename));
    		while((line = in.readLine()) != null){
    			PhoneNumber tmp = new PhoneNumber(line.substring(0,12));
    			String value = line.substring(13);
    			addEntry(tmp, value);   			
    		}
    		in.close();
    	}
    	catch(IOException e){
    		System.out.println(e.toString());
    	}
    }
           
    // Returns the name associated with the given PhoneNumber, if it is
    // in the PhoneBook, null if it is not.
    public String numberLookup(PhoneNumber number){
    	return theBook.getValue(number);
    }
       
    // Returns the PhoneNumber associated with the given name value.
    // There may be duplicate values, return the first one found.
    // Return null if the name is not in the PhoneBook.
    public PhoneNumber nameLookup(String name){
    	return theBook.getKey(name);
    }
       
    // Adds a new PhoneNumber = name pair to the PhoneBook.  All
    // names should be in the form "Last, First".
    // Duplicate entries are *not* allowed.  Return true if the
    // insertion succeeds otherwise false (PhoneBook is full or
    // the new record is a duplicate).  Does not change the datafile on disk.
    public boolean addEntry(PhoneNumber number, String name){
    	return theBook.add(number,name);
    }
       
    // Deletes the record associated with the PhoneNumber if it is
    // in the PhoneBook.  Returns true if the number was found and
    // its record deleted, otherwise false.  Does not change the datafile on disk.
    public boolean deleteEntry(PhoneNumber number){
    	return theBook.delete(number);
    }
       
    // Prints a directory of all PhoneNumbers with their associated
    // names, in sorted order (ordered by PhoneNumber).
    public void printAll(){
    	Iterator<PhoneNumber> iterator = theBook.keys();
    	while(iterator.hasNext()){
    		PhoneNumber tmp = iterator.next();
    		System.out.println(tmp.toString() + ": " + theBook.getValue(tmp));
    	}
    }
       
    // Prints all records with the given Area Code in ordered
    // sorted by PhoneNumber.
    public void printByAreaCode(String code){
    	Iterator<PhoneNumber> iterator = theBook.keys();
    	while(iterator.hasNext()){
    		PhoneNumber tmp = iterator.next();
    		if(code.compareTo(tmp.areaCode) ==  0)
    			System.out.println(tmp.toString() + ": " + theBook.getValue(tmp));
    	}
    }
   
    // Prints all of the names in the directory, in sorted order (by name,
    // not by number).  There may be duplicates as these are the values.       
    public void printNames(){
    	Iterator<String> iterator = theBook.values();
    	while(iterator.hasNext())
    		System.out.println(iterator.next());
    }
}